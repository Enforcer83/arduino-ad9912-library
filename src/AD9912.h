#ifndef  _AD9912_H_
#define  _AD9912_H_

#include <Arduino.h>
#include <SPI.h>

class AD9912Class {

    public:
        
        AD9912Class(SPIClass& spi, int16_t csPin);
        AD9912Class(SPIClass& spi, int16_t csPin, int32_t sysClk, int16_t update);
        AD9912Class(SPIClass& spi, int16_t csPin, int32_t sysClk, int16_t update, int16_t sleep, int16_t rst);
        AD9912Class(SPIClass& spi, int16_t csPin, int32_t sysClk, int16_t clkSel, int16_t update, int16_t sleep, int16_t rst);
        virtual ~AD9912Class();
        
        int16_t begin();
        int16_t end();
        
        // Frequency and Phase functions
        uint64_t convertFloatFreq(double freq);
        uint16_t convertFloatPhase(double phase, bool isDeg)
        int16_t setFreq(uint64_t freqSetPoint);
        int16_t setPhase(uint16_t phaseSetPoint);
        uint64_t readFreq(void);
        uint16_t readPhase(void);
        double convertBinFreq(uint64_t freq);
        double convertBinPhase(uint16_t phase, bool inDeg);
        
        // Configuration functions
        int16_t setConfigReg(uint16_t address, uint8_t* data, size_t length);
        int16_t setConfigReg(uint16_t address, uint8_t* data, size_t length, bool ovrdUpdate);
        int16_t readConfigReg(uint16_t address, uint8_t* data, size_t length);
        
        // change sampling frequency
        // change dac reference current
        
        // Hardware functions
        int16_t resetDDS(void);
        int16_t updateIO(void);
        int16_t sleepChip(void);
        int16_t wakeChip(void);
        int16_t selectClk(void);
        
        // Debug functions
        int16_t error(int16_t errorCode);
        
    private:
    
        SPIClass* _spi;
        int16_t _csPin;
        
        SPISettings _spiSettings;
        
        int32_t _sysClk = 25e6;
        int16_t _clkSel = -1;
        int16_t _update = -1;
        int16_t _sleep = -1;
        int16_t _rst = -1;
        int16_t _errorCode = 0;
        int16_t _clkSelState = LOW;
        
        int32_t _fs = 1e9;
        
        int16_t _rSet = 10e3;                       // Recommended Rset value from datasheet
        int16_t _dac_I_FS = 20000;                  // This value is in microamps
        int16_t _refDAC_I = 120;                    // This value is in microamps
        
        bool _serialAvail = false;
        bool _verbose = false;
        
        //
        // Class initiated Storage locations for each register value
        //
        /*
        uint8_t regSerialConf = 0x18;
        const uint16_t regPartID = 0x1902;
        uint16_t regSerialOp = 0;
        uint8_t regPwrDnEn = 0xC0;
        uint16_t regReset = 0;
        uint8_t regClkDivN = 0x12;
        uint8_t regClkPLL = 0x04;
        uint32_t regOutDivS = 0x010000;
        uint64_t regFTW0 = 0;
        uint16_t regPTW0 = 0;
        uint8_t regDrvrHSTL = 0x05;
        uint8_t regDrvrCMOS = 0x00;
        uint16_t regDAC_FSC = 0x01FF;
        uint16_t regSpurA1 = 0;
        uint16_t regSpurA2 = 0;
        uint16_t regSpurB1 = 0;
        uint16_t regSpurB2 = 0;
        */
        
};

#endif