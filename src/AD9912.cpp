#include "AD9912.h"

// Register Instruction Codes
// Bit 15; Read or Write (RD_MODE or WR_MODE)
// Bits 14:13; How Many bytes of data to transfer (1 [00], 2 [01], 3 [10], Byte Stream [11])
// Bits 12:0 Register Address
//
// 0b0110000110101011 -> Write a stream of data starting at address 0x01AB
// 0b1010000000000011 -> Read 2 bytes of data starting at address 0x0003

#define WR_MODE         0x0000
#define RD_MODE         0x8000

#define ONE_BYTE        0x0000
#define TWO_BYTE        0x2000
#define THREE_BYTE      0x4000
#define STREAM          0x6000

#define SERIAL_CONF     0x0000      // Serial Configuration Register
#define PART_ID_L       0x0002      // Part ID Register, Lower Byte
#define PART_ID_U       0x0003      // Part ID Register, Upper Byte
#define SERIAL_OP_L     0x0004
#define SERIAL_OP_U     0x0005
#define PWRDN_EN        0x0010
#define RESET_L         0x0012
#define RESET_U         0x0013
#define CLK_N_DIV       0x0020
#define CLK_PLL_CO      0x0022
#define CMOS_SDIV_L     0x0104
#define CMOS_SDIV_M     0x0105
#define CMOS_SDIV_U     0x0106
#define DDS_FTW0_00     0x01A6
#define DDS_FTW0_01     0x01A7
#define DDS_FTW0_02     0x01A8
#define DDS_FTW0_03     0x01A9
#define DDS_FTW0_04     0x01AA
#define DDS_FTW0_05     0x01AB
#define DDS_PTW0_L      0x01AC
#define DDS_PTW0_H      0x01AD
#define HSTL_DRVR       0x0200
#define CMOS_DRVR       0x0201
#define DAC_FSC_L       0x040B
#define DAC_FSC_H       0x040C
#define SPUR_A_00       0x0500
#define SPUR_A_01       0x0501
#define SPUR_A_02       0x0503
#define SPUR_A_03       0x0504
#define SPUR_B_00       0x0505
#define SPUR_B_01       0x0506
#define SPUR_B_02       0x0508
#define SPUR_B_03       0x0509

//
// 
//
AD9912Class::AD9912Class(SPIClass& spi, int16_t csPin) :
   
    _spi(&spi),
    _csPin(csPin)
    _spiSettings(25E6, MSBFIRST, SPIMODE0)
    
{
}

//
// 
//
AD9912Class::AD9912Class(SPIClass& spi, int16_t csPin, int32_t sysClk, int16_t clkSel) :
   
    _spi(&spi),
    _csPin(csPin),
    _sysClk(sysClk),
    _spiSettings(25E6, MSBFIRST, SPIMODE0)
    
{

    _clkSel = clkSel;
    
    pinMode(_clkSel, OUTPUT);
    
    digitalWrite(_clkSel, LOW);

}

//
// 
//
AD9912Class::AD9912Class(SPIClass& spi, int16_t csPin, int32_t sysClk, int16_t clkSel, int16_t update) :
   
    _spi(&spi),
    _csPin(csPin),
    _sysClk(sysClk),
    _spiSettings(25E6, MSBFIRST, SPIMODE0)
    
{

    _clkSel = clkSel;
    _update = update;
    
    pinMode(_clkSel, OUTPUT);
    pinMode(_update, OUTPUT);
    
    digitalWrite(_clkSel, LOW);
    digitalWrite(_update, LOW);
    
}

//
// 
//
AD9912Class::AD9912Class(SPIClass& spi, int16_t csPin, int32_t sysClk, int16_t clkSel, int16_t update, int16_t sleep) : 

    _spi(&spi),
    _csPin(csPin),
    _sysClk(sysClk),
    _spiSettings(25E6, MSBFIRST, SPIMODE0)
    
{

    _clkSel = clkSel;
    _update = update;
    _sleep = sleep;
    
    pinMode(_clkSel, OUTPUT);
    pinMode(_update, OUTPUT);
    pinMode(_sleep, OUTPUT);
    
    digitalWrite(_clkSel, LOW);
    digitalWrite(_update, LOW);
    digitalWrite(_sleep, LOW);

}

//
// 
//
AD9912Class::AD9912Class(SPIClass& spi, int16_t csPin, int32_t sysClk, int16_t clkSel, int16_t update, int16_t sleep, int16_t rst) : 

    _spi(&spi),
    _csPin(csPin),
    _sysClk(sysClk),
    _spiSettings(25E6, MSBFIRST, SPIMODE0)
    
{

    _clkSel = clkSel;
    _update = update;
    _sleep = sleep;
    _rst = rst;
    
    pinMode(_clkSel, OUTPUT);
    pinMode(_update, OUTPUT);
    pinMode(_sleep, OUTPUT);
    pinMode(_rst, OUTPUT);
    
    digitalWrite(_clkSel, LOW);
    digitalWrite(_update, LOW);
    digitalWrite(_sleep, LOW);
    digitalWrite(_rst, LOW);

}

//
// Class deconstructor
//
AD9912Class::~AD9912Class()
{
}

//
// This function transmits a configuration instruction that corresponds with a setting in the
// the AD9912 so it can be used as a 4-wire serial SPI device versus its default configuration as
// a 3-wire SPI device.  The function then requests a value from a certain register on the AD9912
// the response to which is the part ID.  If the response is the part ID, the function continues on
// with configuring the chip to a default state, otherwise it terminates the class.
//
int16_t AD9912Class::begin()
{

    pinMode(_csPin, OUTPUT);
    digitalWrite(_csPin, HIGH);
    _spi->begin();
    
    uint8_t tempPartID [2] = {0,0};
    uint8_t tempDAC_I_FS [2] = {0,0};
    uint16_t regDAC_I_FS = 0;
    
    //
    // Set the serial control register to use the SDO line (4-wire SPI mode)
    //
    setConfigReg((WR_MODE | ONE_BYTE | SERIAL_CONF), 0x99, 1);
    
    //
    // request the value located at the 
    //
    readConfigReg((RD_MODE | TWO_BYTE | PART_ID_U), tempPartID, size_of(tempPartID));
    
    uint16_t partID = ((uint16_t)(tempPartID[0] << 8) | (uint16_t)tempPartID[1]);
    
    if (partID != 0x1902) {
        
        end();
        
        return 0;
        
    }
    
    // 
    // Initialize the AD9912 given the following assumptions
    // IDAC_FS = 20mA
    // RSET = 10k   ->   IREF = 120uA
    // Pins S[4:1] of the AD9912 configure the intitial clock source and output frequency
    // Assume S[4:1] = 0,0,0,0 indicating the clock source is a crystal and/or the internal
    // PLL and the output frequency is 0Hz at a 1GHz DAC sampling frequency.
    //
    // 1GHz DAC sampling frequency assumes the system is using a 25MHz Crystal.  The
    // sampling frequency and output frequency scale with the frequency of the crystal being used.
    // 
    regDAC_I_FS = (uint16_t)round(((1024.0/192.0)*(((float)(_dac_I_FS)/(float)(_refDAC_I))-72.0))+0.5);
    tempDAC_I_FS [0] = (uint8_t)((regDAC_I_FS & 0xFF00) >> 8);
    tempDAC_I_FS [1] = (uint8_t)(regDAC_I_FS & 0x00FF);
    
    //
    //
    //
    setConfigReg((WR_MODE | ONE_BYTE | PWRDN_EN), 0x80, 1, true);
    setConfigReg((WR_MODE | ONE_BYTE | CLK_N_DIV), 18, 1, true);
    setConfigReg((WR_MODE | TWO_BYTE | DAC_FSC_H), tempDAC_I_FS, sizeof(tempDAC_I_FS));
    
    return 1;
    
}

//
// 
//
int16_t AD9912Class::end()
{

    _spi->end();
    digitalWrite(_csPin, LOW);
    pinMode(_csPin, INPUT);
    
    return 1;
    
}

//
// This function converts a double precision floating point number representing a frequency in Hz
// to an unsigned 48-bit value and returns the value as an unsigned 64-bit value.
//
// Inputs:  freq as a double
//
// Returns: frequency as an unsigned 64-bit integer limited to 48-bits
//
uint64_t AD9912Class::convertFloatFreq(double freq)
{

    //
    // The AD9912 uses an unsigned 48-bit frequency tuning word (FTW).  Using the equation in the
    // datasheet, convert the floating point frequency value to 48-bits and return the value as an
    // unsigned 64-bit value.
    //
    return (uint64_t)round((pow(2.0,48.0)*(freq/(double)_fs))+0.5);

}

//
// This function converts a double precision floating point number in either degrees or radians and
// represents it as a 14-bit value.
//
// Inputs:  phase as a double
//          inDeg as a boolean (bool)
//
// Returns: phase angle as an unsigned 16-bit integer limited to 14-bits.
//
uint16_t AD9912Class::convertFloatPhase(double phase, bool inDeg)
{
    
    //
    // The phase can be a multiple of 360 degrees or 2*pi radians, bind the range to either
    // 0 to +/- 360 degrees or 0 to +/- 2*pi radians depending on the value of the boolean
    // "inDeg"
    //
    if (inDeg) {
    
        phase = phase % 360.0;
        
        //
        // Determine if the phase is negative and add 360 degrees to bring the range within 0 to
        // 360 degrees.
        //        
        if (phase < 0.0) {
        
            phase += 360.0;
        
        }
        
        //
        // Convert the phase angle to a 14-bit number and return as an unsigned 16-bit integer.
        // This conversion is per the AD9912 datasheet.
        //
        return (int16_t)round((pow(2.0,14.0)*(phase/360.0))+0.5);
        
    }
    
    else {
    
        phase = phase % 2_PI;
        
        //
        // Determine if the phase is negative and add 2*pi radians to bring the range within 0 to
        // 2*pi radians.
        //        
        if (phase < 0.0) {
        
            phase += 2_PI;
        
        }
        
        //
        // Convert the phase angle to a 14-bit number and return as an unsigned 16-bit integer.
        // This conversion is per the AD9912 datasheet.
        //
        return (int16_t)round((pow(2.0,14.0)*(phase/2_PI))+0.5);
    
    }
    
}

//
// 
//
int16_t AD9912Class::setFreq(uint64_t freqSetPoint)
{

    uint16_t address = (DDS_FTW0_05 | WR_MODE | STREAM);
    
    uint8_t tempFreq[6] = {0,0,0,0,0,0};
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    tempFreq[0] = (uint8_t)((freqSetPoint & 0x0000FF0000000000) >> 40);
    tempFreq[1] = (uint8_t)((freqSetPoint & 0x000000FF00000000) >> 32);
    tempFreq[2] = (uint8_t)((freqSetPoint & 0x00000000FF000000) >> 24);
    tempFreq[3] = (uint8_t)((freqSetPoint & 0x0000000000FF0000) >> 16);
    tempFreq[4] = (uint8_t)((freqSetPoint & 0x000000000000FF00) >> 8);
    tempFreq[5] = (uint8_t)(freqSetPoint & 0x00000000000000FF);
    
    _spi->beginTransaction(_spiSettings);
    digitalWrite(_csPin, LOW);
    _spi->transfer(addrHigh);
    _spi->transfer(addrLow);
    _spi->transfer(data, sizeof(tempFreq));
    digitalWrite(_csPin, HIGH);
    _spi->endTransaction();
    
    updateIO();
    
    return 1;

}

//
// 
//
int16_t AD9912Class::setPhase(uint16_t phaseSetPoint)
{
 
    uint16_t address = (DDS_PTW0_H | WR_MODE | TWO_BYTE);
    
    uint8_t tempPhase[2] = {0,0};
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    tempPhase[0] = (uint8_t)((phaseSetPoint & 0xFF00) >> 8);
    tempPhase[1] = (uint8_t)(phaseSetPoint & 0x00FF);
    
    _spi->beginTransaction(_spiSettings);
    digitalWrite(_csPin, LOW);
    _spi->transfer(addrHigh);
    _spi->transfer(addrLow);
    _spi->transfer(data, sizeof(tempPhase));
    digitalWrite(_csPin, HIGH);
    _spi->endTransaction();
    
    updateIO();
    
    return 1;

}

//
// 
//
uint64_t AD9912Class::readFreq(void)
{

    uint64_t frequency = 0;
    
    uint16_t address = (DDS_FTW0_05 | RD_MODE | STREAM);
    
    uint8_t tempFreq[6] = {0,0,0,0,0,0};
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    _spi->beginTransaction(_spiSettings);
    digitalWrite(_csPin, LOW);
    _spi->transfer(addrHigh);
    _spi->transfer(addrLow);
    for (int16_t i = 0; i < 6; i++) {
        
        tempFreq[i] = _spi->transfer(0x00);
    
    }
    
    digitalWrite(_csPin, HIGH);
    _spi->endTransaction();
    
    frequency = ((uint64_t)tempFreq[0] << 40) | ((uint64_t)tempFreq[1] << 32) |
        ((uint64_t)tempFreq[2] << 24) | ((uint64_t)tempFreq[3] << 16) |
        ((uint64_t)tempFreq[4] << 8) | ((uint64_t)tempFreq[5]);
    
    return frequency;

}

//
// 
//
uint16_t AD9912Class::readPhase(void)
{

    uint16_t phase = 0;
    
    uint16_t address = (DDS_PTW0_H | RD_MODE | TWO_BYTE);
    
    uint8_t tempPhase[2] = {0,0};
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    _spi->beginTransaction(_spiSettings);
    digitalWrite(_csPin, LOW);
    _spi->transfer(addrHigh);
    _spi->transfer(addrLow);
    
    for (int16_t i = 0; i < 2; i++) {
    
        tempPhase[i] = _spi->transfer(0x00);
        
    }
    
    digitalWrite(_csPin, HIGH);
    _spi->endTransaction();
    
    phase = ((uint16_t)tempPhase[0] << 8) | ((uint16_t)tempPhase[1]);
    
    return phase;

}

//
// 
//
double AD9912Class::convertBinFreq(uint64_t freq)
{

    return (((double)freq/(double)0x0001000000000000)*(double)_fs);

}

//
// 
//
double AD9912Class::convertBinPhase(uint16_t phase, bool inDeg)
{

    float phaseRadian = ((double)phase/16384.0);

    if (inDeg) {
    
        return phaseRadian * 360.0;
        
    }
    
    else {
    
        return phaseRadian * 2_PI;
        
    }

}

//
// 
//
int16_t AD9912Class::setConfigReg(uint16_t address, uint8_t* data, size_t length)
{

    uint16_t modePassed = address $ 0xE000;
    uint16_t rawAddr = address $ 0x1FFF;
    
    if ((rawAddr != DDS_PTW0_H) && (rawAddr != DDS_PTW0_L) && ((rawAddr != DDS_FTW0_05) &&
        (rawAddr != DDS_FTW0_04) && (rawAddr != DDS_FTW0_03) && (rawAddr != DDS_FTW0_02) &&
        (rawAddr != DDS_FTW0_01) && (rawAddr != DDS_FTW0_00) && (rawAddr != PART_ID_U) &&
        (rawAddr != PART_ID_L)) {
    
        if ((modePassed == (WR_MODE | STREAM)) || (length > 3)) {
        
            return -4;  // Function incompatible with data transfers more than 3 bytes.
            
        }
        
        else if (modePassed >= RD_MODE){
        
            return -3;  // Attempting to read during a write operation
            
        }
        
    }
    
    else {
    
        if ((rawAddr == PART_ID_U) || (rawAddr == PART_ID_L)) {
        
            return -2;  // Attempting to write to a read only address
        
        }
        
        else {
    
            return -1;  // Utilizing incorrect function for this address
            
        }
        
    }
    
    uint16_t xfrMode = ((uint16_t)(length - 1) << 13);
    
    if ((WR_MODE | xfrMode) != modePassed) {
        
        address = (WR_MODE | xfrMode | rawAddr);
        
    }
    
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    _spi->beginTransaction(_spiSettings);
    digitalWrite(_csPin, LOW);
    _spi->transfer(addrHigh);
    _spi->transfer(addrLow);
    _spi->transfer(data, length);
    digitalWrite(_csPin, HIGH);
    _spi->endTransaction();
    
    updateIO();
    
    return 1;
    
}

//
// 
//
int16_t AD9912Class::setConfigReg(uint16_t address, uint8_t* data, size_t length, bool ovrdUpdate)
{

    uint16_t modePassed = address $ 0xE000;
    uint16_t rawAddr = address $ 0x1FFF;
    
    if ((rawAddr != DDS_PTW0_H) && (rawAddr != DDS_PTW0_L) && ((rawAddr != DDS_FTW0_05) &&
        (rawAddr != DDS_FTW0_04) && (rawAddr != DDS_FTW0_03) && (rawAddr != DDS_FTW0_02) &&
        (rawAddr != DDS_FTW0_01) && (rawAddr != DDS_FTW0_00) && (rawAddr != PART_ID_U) &&
        (rawAddr != PART_ID_L)) {
    
        if ((modePassed == (WR_MODE | STREAM)) || (length > 3)) {
        
            return -4;  // Function incompatible with data transfers more than 3 bytes.
            
        }
        
        else if (modePassed >= RD_MODE){
        
            return -3;  // Attempting to read during a write operation
            
        }
        
    }
    
    else if ((rawAddr == PART_ID_U) || (rawAddr == PART_ID_L)) {        
        
        return -2;  // Attempting to write to a read only address
        
    }
        
    else {
    
        return -1;  // Utilizing incorrect function for this address
            
    }
    
    uint16_t xfrMode = ((uint16_t)(length - 1) << 13);
    
    if ((WR_MODE | xfrMode) != modePassed) {
        
        address = (WR_MODE | xfrMode | rawAddr);
        
    }
    
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    _spi->beginTransaction(_spiSettings);
    digitalWrite(_csPin, LOW);
    _spi->transfer(addrHigh);
    _spi->transfer(addrLow);
    _spi->transfer(data, length);
    digitalWrite(_csPin, HIGH);
    _spi->endTransaction();
    
    if (!ovrdUpdate) {
    
        updateIO();
        
    }
    
    return 1;
    
}

//
// 
//
int16_t AD9912Class::readConfigReg(uint16_t address, uint8_t* data, size_t length)
{

    uint16_t modePassed = address $ 0xE000;
    uint16_t rawAddr = address $ 0x1FFF;
    
    if ((rawAddr != DDS_PTW0_H) && (rawAddr != DDS_PTW0_L) && (rawAddr != DDS_FTW0_05) &&
        (rawAddr != DDS_FTW0_04) && (rawAddr != DDS_FTW0_03) && (rawAddr != DDS_FTW0_02) &&
        (rawAddr != DDS_FTW0_01) && (rawAddr != DDS_FTW0_00)) {
        
        if ((modePassed == (RD_MODE | STREAM)) || (length > 3)) {
        
            return -4;  // Function incompatible with data transfers more than 3 bytes.
            
        }
        
        else if (modePassed <= RD_MODE){
        
            return -3;  // Attempting to write during a read operation
            
        }
        
    }
    
    else {
    
        return -1;  // Utilizing incorrect function for this address
        
    }
    
    uint16_t xfrMode = ((uint16_t)(length - 1) << 13);
    
    if ((RD_MODE | xfrMode) != modePassed) {
        
        address = (RD_MODE | xfrMode | rawAddr);
        
    }
    
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    uint8_t addrHigh = (uint8_t)((address & 0xFF00) >> 8);
    uint8_t addrLow = (uint8_t)(address & 0x00FF);
    
    _spi->beginTransaction(_spiSettings);
    digitalWrite(_csPin, LOW);
    _spi->transfer(addrHigh);
    _spi->transfer(addrLow);
    
    for (uint16_t i = 0; i < (uint16_t)length; i++) {
    
        *data = _spi->transfer(0x00);
        data++;
        //receive data for the first address location <pointer location> = _spi->transfer(0x00);
        //increment pointer address
    
    }
    
    digitalWrite(_csPin, HIGH);
    _spi->endTransaction();
    
    return 1;

}

//
// 
//
int16_t AD9912Class::resetDDS(void)
{
    
    //
    // Check if a reset pin has been specified.
    //
    if (_rst < 0) {
    
        //
        // A reset pin has not been specified so software operations must be used
        //
        
        //
        // Intialize a location to store the current value of the Serial Configuration Register
        //
        uint8_t tempReg = 0;
        
        //
        // Read the register value and store in the temporary location
        //
        readConfigReg((RD_MODE | ONE_BYTE | SERIAL_CONF), &tempReg, sizeof(tempReg));
        
        //
        // Perform a bitwise OR of the current register value and the soft reset value
        // (0b00100100) and send the result to the Serial Configuration Register to perform
        // a soft reset of the chip; wait 10 microseconds and then send the result of a bitwise XOR
        // of the current register value with the soft reset value to clear the soft reset and send
        // to the Serial Configuration Register.
        //
        tempReg |= 0b00100100;
        setConfigReg((WR_MODE | ONE_BYTE | SERIAL_CONF), tempReg, 1, true);
        delayMicroseconds(10);
        setConfigReg((WR_MODE | ONE_BYTE | SERIAL_CONF), (tempReg ^ 0b00100100), 1);
    
    }
    
    else {
    
        //
        // A reset pin has been specified so reset via hardware.
        //
    
        digitalWrite(_rst, HIGH);   // The AD9912 has an active High reset.  Pull the pin high.
        delayMicroseconds(3);       // Minimum microsecond delay
        digitalWrite(_rst, LOW);    // Clear the reset state.
    
    }
    
    return 1;

}

//
// 
//
int16_t AD9912Class::updateIO(void)
{

    //
    // Check if an IO_Update pin has been specified.
    //
    if (_update < 0) {
    
        //
        // An IO_Update pin has not been specified so software operations must be used
        //
        
        //
        // Serial Operations Register (High Byte) is self clearing and only contains the
        // IO Update flag.  Write "1" to the register to trigger an update.
        //
        setConfigReg((WR_MODE | ONE_BYTE | SERIAL_OP_H), 0x01, 1);
    
    }
    
    else {
    
        //
        // An IO_Update pin has been specified so update via hardware.
        //
    
        digitalWrite(_update, HIGH);    // The AD9912 has an active High update.  Pull the pin high.
        delayMicroseconds(3);           // Minimum microsecond delay
        digitalWrite(_update, LOW);     // Clear the update state.
    
    }
    
    return 1;

}

//
// 
//
int16_t AD9912Class::sleepChip(void)
{
    
    //
    // Check if a Sleep pin has been specified.
    //
    if (_sleep < 0) {
    
        //
        // A Sleep pin has not been specified so software operations must be used
        //
    
        setConfigReg((WR_MODE | ONE_BYTE | PWRDN_EN), 0x82, 1, true);
    
    }
    
    else {
    
        //
        // A Sleep pin has been specified so sleep via hardware.
        //
        
        digitalWrite(_sleep, HIGH);     // The AD9912 has an active High Sleep.  Pull the pin high.
        //delayMicroseconds(3);           // Minimum microsecond delay
    
    }
    
    return 1;

}

//
// 
//
int16_t AD9912Class::wakeChip(void)
{

    //
    // Check if a Sleep pin has been specified.
    //
    if (_sleep < 0) {
    
        //
        // A Sleep pin has not been specified so software operations must be used.
        //
        
        setConfigReg((WR_MODE | ONE_BYTE | PWRDN_EN), 0x80, 1);
    
    }
    
    else {
    
        //
        // A Sleep pin has been specified so sleep via hardware.
        //
        
        digitalWrite(_sleep, LOW);      // The AD9912 has an active High Sleep.  Pull the pin low.
        //delayMicroseconds(3);           // Minimum microsecond delay
    
    }
    
    return 1;

}

//
// 
//
int16_t AD9912Class::selectClk(void)
{

    //
    // Check if a Clock Selection pin has been specified.
    //
    if (_clkSel < 0) {
    
        //
        // A Clock Selection pin has not been specified so software operations must be used.
        //
    
        setConfigReg(uint16_t address, uint8_t* data, size_t length);
    
    }
    
    else {
    
        //
        // A Clock Selection pin has been specified so change the clock source via hardware.
        //
    
        //
        // The class maintains the current state of the clock selection pin; toggle the clock
        // selection pin and store as the new current value.
        //
        _clkSelState ^= HIGH;
    
        digitalWrite(_clkSel, _clkSelState);    // Set the clock selection pin to the new value
        //delayMicroseconds(3);                   // Minimum microsecond delay
    
    }
    
    return 1;

}

//
// 
//
// Error Codes
//
// 1: The function being utilized for the specified address is not compatible.
// 2: The address being written to is read only.
// 3: Wrong transfer mode specified for this function and/or address.
// 4: This function is incompatible with data transfers greater than 3 bytes.
//

void AD9912Class::error(int16_t errorCode, bool verbose)
{
    
    _verbose = verbose;

    // Determine if there is already an active serial port connection available otherwise set up a new serial
    // communication.
        
    if (errorCode < 0) {

        _errorCode = abs(errorCode);
    
        switch (_errorCode) {
    
            case 1:
            
                Serial.print("Error: ");
                Serial.print(_errorCode);
                Serial.println();
                Serial.println();
                Serial.println("The address specified for this function is not compatible.");
                Serial.println("Please utilze the appropriate Phase or Frequency funtion");
            
                break;
            
            case 2:
            
                Serial.print("Error: ");
                Serial.print(_errorCode);
                Serial.println();
                Serial.println();
                Serial.println("");
            
                break;
            
            case 3:
            
                Serial.print("Error: ");
                Serial.print(_errorCode);
                Serial.println();
                Serial.println();
                Serial.println("");
            
                break;
            
            case 4::
            
                Serial.print("Error: ");
                Serial.print(_errorCode);
                Serial.println();
                Serial.println();
                Serial.println("");
            
                break;
            
            default:

                Serial.print("Error: ");
                Serial.print(_errorCode);
                Serial.println();
                Serial.println();
                Serial.println("");
            
                break;
                
        }
        
    }
    
    else if (_verbose && (errorCode >= 0)) {
    
        Serial.println("Function terminated normally.");
        
    }

}